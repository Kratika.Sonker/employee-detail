package com.employee.detail.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transport_data")
public class Transport {

	@Id
	private Integer vehicleId;
	@Column(name="vehicle_name")
	private String vehicleName;
	@Column(name="vehicle_no")
	private String vehicleNo;
	@Column(name="vehicle_source")
	private String vehicleSource;
	@Column(name="vehicle_destination")
	private String vehicleDestination;
	public Transport() {
		super();
	}
	public Transport(Integer vehicleId, String vehicleName, String vehicleNo, String vehicleSource,
			String vehicleDestination) {
		super();
		this.vehicleId = vehicleId;
		this.vehicleName = vehicleName;
		this.vehicleNo = vehicleNo;
		this.vehicleSource = vehicleSource;
		this.vehicleDestination = vehicleDestination;
	}
	public Integer getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getVehicleSource() {
		return vehicleSource;
	}
	public void setVehicleSource(String vehicleSource) {
		this.vehicleSource = vehicleSource;
	}
	public String getVehicleDestination() {
		return vehicleDestination;
	}
	public void setVehicleDestination(String vehicleDestination) {
		this.vehicleDestination = vehicleDestination;
	}
	@Override
	public String toString() {
		return "Transport [vehicleId=" + vehicleId + ", vehicleName=" + vehicleName + ", vehicleNo=" + vehicleNo
				+ ", vehicleSource=" + vehicleSource + ", vehicleDestination=" + vehicleDestination + "]";
	}
	
	
}

